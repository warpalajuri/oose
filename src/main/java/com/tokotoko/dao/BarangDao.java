package com.tokotoko.dao;

import com.tokotoko.model.Barang;
import com.tokotoko.utils.DatabaseConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by warpalajuri on 23/05/16.
 */
public class BarangDao {
    private PreparedStatement preparedStatement = null;

    public BarangDao() {

    }

    public List<Barang> getAllBarang() throws SQLException {
        String query = "SELECT * FROM barang;";
        ResultSet resultSet;
        List<Barang> barangList = new ArrayList<>();
        try {
            DatabaseConnection.getInstance().connectToDatabase();
            preparedStatement = DatabaseConnection
                    .getInstance()
                    .getConnection()
                    .prepareStatement(query);
            resultSet = preparedStatement.executeQuery(query);
            while (resultSet.next()) {
                int bId = resultSet.getInt("barang_id");
                String bNama = resultSet.getString("barang_nama");
                long bHarga = resultSet.getLong("harga");
                String bDeskripsi = resultSet.getString("deskripsi");
                String bStatus = resultSet.getString("status");
                barangList.add(new Barang(bId, bNama, bHarga, bDeskripsi, bStatus));
            }
        } finally {
            // Do something
        }
        return barangList;
    }

    public Barang getBarangFromId(int idBarang) throws SQLException {
        String query = "SELECT * FROM barang WHERE barang_id = " + idBarang + ";";
        ResultSet resultSet;
        Barang barang = null;
        try {
            DatabaseConnection.getInstance().connectToDatabase();
            preparedStatement = DatabaseConnection
                    .getInstance()
                    .getConnection()
                    .prepareStatement(query);
            resultSet = preparedStatement.executeQuery(query);
            while (resultSet.next()) {
                int bId = resultSet.getInt("barang_id");
                String bNama = resultSet.getString("barang_nama");
                long bHarga = resultSet.getLong("harga");
                String bDeskripsi = resultSet.getString("deskripsi");
                String bStatus = resultSet.getString("status");
                barang = new Barang(bId, bNama, bHarga, bDeskripsi, bStatus);
            }
        } finally {
            // Do something
        }
        return barang;
    }

    public List<Barang> getBarangFromName(String namaBarang) throws SQLException {
        String query = "SELECT * FROM barang WHERE barang_nama LIKE '%" + namaBarang + "%';";
        ResultSet resultSet;
        Barang barang;
        List<Barang> barangList = new ArrayList<>();
        try {
            DatabaseConnection.getInstance().connectToDatabase();
            preparedStatement = DatabaseConnection
                    .getInstance()
                    .getConnection()
                    .prepareStatement(query);
            resultSet = preparedStatement.executeQuery(query);
            while (resultSet.next()) {
                int bId = resultSet.getInt("barang_id");
                String bNama = resultSet.getString("barang_nama");
                long bHarga = resultSet.getLong("harga");
                String bDeskripsi = resultSet.getString("deskripsi");
                String bStatus = resultSet.getString("status");
                barang = new Barang(bId, bNama, bHarga, bDeskripsi, bStatus);
                barangList.add(barang);
            }
        } finally {
            // Do something
        }
        return barangList;
    }

    public void addBarang(Barang barang) throws SQLException {
        String bNama = barang.getNama();
        long bHarga = barang.getHarga();
        String bDeskripsi = barang.getDeskripsi();
        String bStatus = barang.getStatus();
        String query = "INSERT INTO barang (barang_nama, harga, deskripsi, status)\n" +
                "VALUES ('" + bNama + "'," + bHarga + ",'" + bDeskripsi + "','" + bStatus + "');";
        try {
            DatabaseConnection.getInstance().connectToDatabase();
            preparedStatement = DatabaseConnection
                    .getInstance()
                    .getConnection()
                    .prepareStatement(query);
            preparedStatement.executeUpdate();
        } finally {
            // Do something
        }
    }

    public void editBarang(Barang barang) throws SQLException {
        int bId = barang.getId();
        String bNama = barang.getNama();
        long bHarga = barang.getHarga();
        String bDeskripsi = barang.getDeskripsi();
        String bStatus = barang.getStatus();
        String query = "UPDATE barang \n" +
                "SET barang_nama = " + bNama + "," +
                "harga = " + bHarga + "," +
                "deskripsi = " + bDeskripsi + "," +
                "status = " + bStatus +
                "WHERE barang_id = " + bId + ";";
        try {
            DatabaseConnection.getInstance().connectToDatabase();
            preparedStatement = DatabaseConnection
                    .getInstance()
                    .getConnection()
                    .prepareStatement(query);
            preparedStatement.executeUpdate();
        } finally {
            // Do something
        }
    }

    public void deleteBarang(Barang barang) throws SQLException {
        int bId = barang.getId();
        String query = "DELETE barang WHERE barang_id = " + bId + ";";
        try {
            DatabaseConnection.getInstance().connectToDatabase();
            preparedStatement = DatabaseConnection
                    .getInstance()
                    .getConnection()
                    .prepareStatement(query);
            preparedStatement.executeUpdate();
        } finally {
            // Do something
        }
    }
}
