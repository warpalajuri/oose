package com.tokotoko.dao;

import com.tokotoko.model.Barang;
import com.tokotoko.model.Penjual;
import com.tokotoko.utils.DatabaseConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by warpalajuri on 22/05/16.
 */
public class PenjualDao {
    private PreparedStatement preparedStatement = null;

    public PenjualDao() {

    }

    public List<Penjual> getAllPenjual() throws SQLException {
        String query = "SELECT p.*, b.*\n" +
                "FROM penjual p\n" +
                "INNER JOIN penjual_barang pb\n" +
                "ON pb.penjual_id = p.penjual_id\n" +
                "INNER JOIN barang b\n" +
                "ON b.barang_id = pb.barang_id;";
        ResultSet resultSet;
        List<Penjual> penjualList = new ArrayList<>();
        try {
            DatabaseConnection.getInstance().connectToDatabase();
            preparedStatement = DatabaseConnection
                    .getInstance()
                    .getConnection()
                    .prepareStatement(query);
            resultSet = preparedStatement.executeQuery(query);
            while (resultSet.next()) {
                int pId = resultSet.getInt("penjual_id");
                String pNama = resultSet.getString("penjual_nama");
                String pKontak = resultSet.getString("kontak");
                String pAlamat = resultSet.getString("alamat");
                Integer bId = resultSet.getInt("barang_id");
                if (bId == null) {
                    penjualList.add(new Penjual(pId, pNama, pKontak, pAlamat));
                } else {
                    String bNama = resultSet.getString("barang_nama");
                    long bHarga = resultSet.getLong("harga");
                    String bDeskripsi = resultSet.getString("deskripsi");
                    String bStatus = resultSet.getString("status");
                    Barang barang = new Barang(bId, bNama, bHarga, bDeskripsi, bStatus);
                    List<Barang> barangList = new ArrayList<>();
                    barangList.add(barang);
                    penjualList.add(new Penjual(pId, pNama, pKontak, pAlamat, barangList));
                }
            }
        } finally {
            // Do something
        }
        return penjualList;
    }

    public Penjual getPenjualFromId(int idPenjual) throws SQLException {
        String query = "SELECT p.*, b.*\n" +
                "FROM penjual p\n" +
                "INNER JOIN penjual_barang pb\n" +
                "ON pb.penjual_id = p.penjual_id\n" +
                "INNER JOIN barang b\n" +
                "ON b.barang_id = pb.barang_id\n" +
                "WHERE p.penjual_id = " + idPenjual + ";";
        ResultSet resultSet;
        Penjual penjual = null;
        try {
            DatabaseConnection.getInstance().connectToDatabase();
            preparedStatement = DatabaseConnection
                    .getInstance()
                    .getConnection()
                    .prepareStatement(query);
            resultSet = preparedStatement.executeQuery(query);
            while (resultSet.next()) {
                int pId = resultSet.getInt("penjual_id");
                String pNama = resultSet.getString("penjual_nama");
                String pKontak = resultSet.getString("kontak");
                String pAlamat = resultSet.getString("alamat");
                Integer bId = resultSet.getInt("barang_id");
                if (bId == null) {
                    penjual = new Penjual(pId, pNama, pKontak, pAlamat);
                } else {
                    String bNama = resultSet.getString("barang_nama");
                    long bHarga = resultSet.getLong("harga");
                    String bDeskripsi = resultSet.getString("deskripsi");
                    String bStatus = resultSet.getString("status");
                    Barang barang = new Barang(bId, bNama, bHarga, bDeskripsi, bStatus);
                    List<Barang> barangList = new ArrayList<>();
                    barangList.add(barang);
                    penjual = new Penjual(pId, pNama, pKontak, pAlamat, barangList);
                }
            }
        } finally {
            // Do something
        }
        return penjual;
    }

    public Penjual getPenjualFromNama(String namaPenjual) throws SQLException {
        String query = "SELECT p.*, b.*\n" +
                "FROM penjual p\n" +
                "INNER JOIN penjual_barang pb\n" +
                "ON pb.penjual_id = p.penjual_id\n" +
                "INNER JOIN barang b\n" +
                "ON b.barang_id = pb.barang_id\n" +
                "WHERE p.penjual_nama LIKE " + "'%" + namaPenjual + "%';";
        ResultSet resultSet;
        Penjual penjual = null;
        try {
            DatabaseConnection.getInstance().connectToDatabase();
            preparedStatement = DatabaseConnection
                    .getInstance()
                    .getConnection()
                    .prepareStatement(query);
            resultSet = preparedStatement.executeQuery(query);
            while (resultSet.next()) {
                int pId = resultSet.getInt("penjual_id");
                String pNama = resultSet.getString("penjual_nama");
                String pKontak = resultSet.getString("kontak");
                String pAlamat = resultSet.getString("alamat");
                Integer bId = resultSet.getInt("barang_id");
                if (bId == null) {
                    penjual = new Penjual(pId, pNama, pKontak, pAlamat);
                } else {
                    String bNama = resultSet.getString("barang_nama");
                    long bHarga = resultSet.getLong("harga");
                    String bDeskripsi = resultSet.getString("deskripsi");
                    String bStatus = resultSet.getString("status");
                    List<Barang> barangList = new ArrayList<>();
                    barangList.add(new Barang(bId, bNama, bHarga, bDeskripsi, bStatus));
                    penjual = new Penjual(pId, pNama, pKontak, pAlamat, barangList);
                }
            }
        } finally {
            // Do something
        }
        return penjual;
    }

    public Penjual getPenjualFromBarangId(int idBarang) throws SQLException {
        String query = "SELECT * FROM penjual_barang WHERE barang_id = " + idBarang + ";";
        ResultSet resultSet;
        Penjual penjual = null;
        try {
            DatabaseConnection.getInstance().connectToDatabase();
            preparedStatement = DatabaseConnection
                    .getInstance()
                    .getConnection()
                    .prepareStatement(query);
            resultSet = preparedStatement.executeQuery(query);
            while (resultSet.next()) {
                Integer pId = resultSet.getInt("penjual_id");
                if (pId == null) {
                    // Uh oh!
                } else {
                    penjual = getPenjualFromId(pId);
                }
            }
        } finally {
            // Do something
        }
        return penjual;
    }

    public void addPenjual(Penjual penjual) throws SQLException {
        String pNama = penjual.getNama();
        String pKontak = penjual.getKontak();
        String pAlamat = penjual.getAlamat();
        String query = "INSERT INTO penjual (penjual_nama, kontak, alamat)\n" +
                "VALUES ('" + pNama + "','" + pKontak + "','" + pAlamat + "');";
        try {
            DatabaseConnection.getInstance().connectToDatabase();
            preparedStatement = DatabaseConnection
                    .getInstance()
                    .getConnection()
                    .prepareStatement(query);
            preparedStatement.executeUpdate();
        } finally {
            // Do something
        }
    }

    public void editPenjualData(Penjual penjual) throws SQLException {
        int pId = penjual.getId();
        String pNama = penjual.getNama();
        String pKontak = penjual.getKontak();
        String pAlamat = penjual.getAlamat();
        String query = "UPDATE penjual \n" +
                "SET penjual_nama = " + pNama + "," +
                "kontak = " + pKontak + "," +
                "alamat = " + pAlamat +
                "WHERE penjual_id = " + pId + ";";
        try {
            DatabaseConnection.getInstance().connectToDatabase();
            preparedStatement = DatabaseConnection
                    .getInstance()
                    .getConnection()
                    .prepareStatement(query);
            preparedStatement.executeUpdate();
        } finally {
            // Do something
        }
    }

    public void deletePenjual(Penjual penjual) throws SQLException {
        int pId = penjual.getId();
        String query = "DELETE penjual WHERE penjual_id = " + pId + ";";
        try {
            DatabaseConnection.getInstance().connectToDatabase();
            preparedStatement = DatabaseConnection
                    .getInstance()
                    .getConnection()
                    .prepareStatement(query);
            preparedStatement.executeUpdate();
        } finally {
            // Do something
        }
    }
}
