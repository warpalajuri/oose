package com.tokotoko.dao;

import com.tokotoko.utils.DatabaseConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by warpalajuri on 24/05/16.
 */
public class InvoiceDao {
    private PreparedStatement preparedStatement;

    public InvoiceDao() {
    }

    public Integer getLatestInvoiceId() throws SQLException {
        String query = "SELECT invoice_id\n" +
                "FROM invoice\n" +
                "ORDER BY invoice_id DESC\n" +
                "LIMIT 1;";
        ResultSet resultSet;
        Integer invoiceId = null;
        try {
            DatabaseConnection.getInstance().connectToDatabase();
            preparedStatement = DatabaseConnection
                    .getInstance()
                    .getConnection()
                    .prepareStatement(query);
            resultSet = preparedStatement.executeQuery(query);
            while (resultSet.next()) {
                    invoiceId = resultSet.getInt("invoice_id");
            }
        } finally {
            // Do something
        }
        return invoiceId;
    }
}
