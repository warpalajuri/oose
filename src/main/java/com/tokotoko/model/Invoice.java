package com.tokotoko.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by warpalajuri on 21/05/16.
 */
public class Invoice {
    private int id;
    private HashMap<Barang, Integer> items;
    private long total;
    private String namaPenjual;
    private String kontakPenjual;
    private String namaPembeli;
    private String alamatPembeli;
    private String kontakPembeli;

    public Invoice(int id, HashMap<Barang, Integer> items, long total, String namaPenjual, String kontakPenjual, String namaPembeli, String alamatPembeli, String kontakPembeli) {
        this.id = id;
        this.items = items;
        this.total = total;
        this.namaPenjual = namaPenjual;
        this.kontakPenjual = kontakPenjual;
        this.namaPembeli = namaPembeli;
        this.alamatPembeli = alamatPembeli;
        this.kontakPembeli = kontakPembeli;
    }

    public Invoice(HashMap<Barang, Integer> items, long total, String namaPenjual, String kontakPenjual, String namaPembeli, String alamatPembeli, String kontakPembeli) {
        this.items = items;
        this.total = total;
        this.namaPenjual = namaPenjual;
        this.kontakPenjual = kontakPenjual;
        this.namaPembeli = namaPembeli;
        this.alamatPembeli = alamatPembeli;
        this.kontakPembeli = kontakPembeli;
    }

    public void printInvoice() {
        System.out.println("----------Invoice-----------");
        System.out.println("ID: " + id);
        System.out.println("Item:");
        for (Map.Entry<Barang, Integer> map : items.entrySet()) {
            String barang = map.getKey().getNama();
            int quantity = map.getValue();
            long itemPrice = map.getKey().getHarga();
            System.out.println(barang + " (Qty: " + quantity + " @" + itemPrice + ")");
        }
        System.out.println("Total: " + total + "\n");
        System.out.println("Nama Penjual: \n" + namaPenjual);
        System.out.println("Kontak Penjual: \n" + kontakPenjual + "\n");
        System.out.println("Nama Pembeli: \n" + namaPembeli);
        System.out.println("Alamat Pembeli: \n" + alamatPembeli);
        System.out.println("Kontak Pembeli: \n" + kontakPembeli);
        System.out.println("----------------------------");
        System.out.println("Terima kasih sudah berbelanja di Tokotoko!");
    }
}
