package com.tokotoko.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by warpalajuri on 21/05/16.
 */
public class Penjual {
    private int id;
    private String nama;
    private String kontak;
    private String alamat;
    private List<Barang> barangList;

    public Penjual(int id, String nama, String kontak, String alamat, List<Barang> barangList) {
        this.id = id;
        this.nama = nama;
        this.kontak = kontak;
        this.alamat = alamat;
        this.barangList = barangList;
    }

    public Penjual(int id, String nama, String kontak, String alamat) {
        this.id = id;
        this.nama = nama;
        this.kontak = kontak;
        this.alamat = alamat;
    }

    public Penjual(String nama, String kontak, String alamat) {
        this.nama = nama;
        this.kontak = kontak;
        this.alamat = alamat;
    }

    public void tambahBarang(Barang barang) {
        if (barangList.isEmpty()) {
            List<Barang> tempList = new ArrayList<>();
            tempList.add(barang);
            setBarangList(tempList);
        } else {
            barangList.add(barang);
        }
    }

    public void editBarang(Barang barang) {
        if (barangList.isEmpty()) {
            System.out.println("Anda tidak punya barang untuk diedit.");
        } else {
            int barangId = barang.getId();
            for (int i = 0; i < barangList.size(); i++) {
                if (barangId == barangList.get(i).getId()) {
                    barangList.set(i, barang);
                } else {
                    System.out.println("Anda tidak punya barang untuk diedit.");
                }
            }
        }
    }

    public void hapusBarang(Barang barang) {
        if (barangList.isEmpty()) {
            System.out.println("Anda tidak punya barang untuk dihapus.");
        } else {
            barangList.remove(barang);
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKontak() {
        return kontak;
    }

    public void setKontak(String kontak) {
        this.kontak = kontak;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public List<Barang> getBarangList() {
        return barangList;
    }

    public void setBarangList(List<Barang> barangList) {
        this.barangList = barangList;
    }
}
