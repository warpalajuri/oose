package com.tokotoko.model;

/**
 * Created by warpalajuri on 21/05/16.
 */
public class Barang {
    public static final String ITEM_AVAILABLE = "Available";
    public static final String ITEM_SOLD_OUT = "Sold Out";
    public static final String ITEM_RESTOCK = "Restock";

    private int id;
    private String nama;
    private long harga;
    private String deskripsi;
    private String status;

    public Barang(int id, String nama, long harga, String deskripsi, String status) {
        this.id = id;
        this.nama = nama;
        this.harga = harga;
        this.deskripsi = deskripsi;
        this.status = status;
    }

    public Barang(String nama, long harga, String deskripsi, String status) {
        this.nama = nama;
        this.harga = harga;
        this.deskripsi = deskripsi;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public long getHarga() {
        return harga;
    }

    public void setHarga(long harga) {
        this.harga = harga;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
