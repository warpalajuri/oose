package com.tokotoko;

import com.tokotoko.dao.BarangDao;
import com.tokotoko.dao.PenjualDao;
import com.tokotoko.model.Barang;
import com.tokotoko.model.Invoice;
import com.tokotoko.model.Penjual;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by warpalajuri on 21/05/16.
 */
public class Application {

    private Application() {
    }

    public static void main(String[] argv) {
        Application application = new Application();
        Cart.getInstance().initCart();
        application.printWelcomeMessage();
        application.home();
    }

    private void printWelcomeMessage() {
        System.out.println("Selamat datang di Tokotoko!");
    }

    private void home() {
        System.out.println("----------------------------");
        System.out.println("(1) Lihat semua barang");
        System.out.println("(2) Cari barang");
        System.out.println("(3) Cari penjual");
        System.out.println("(4) Lihat Cart");
        System.out.println("(5) Checkout");
        System.out.println("(6) Keluar");
        System.out.println("----------------------------");
        System.out.println("Masukkan pilihan Anda:");
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();

        switch (input) {
            case 1:
                getAllBarang();
                break;
            case 2:
                searchBarang();
                break;
            case 3:
                searchPenjual();
                break;
            case 4:
                Cart.getInstance().checkContent();
                checkCartOptions();
                break;
            case 5:
                checkoutAndPrintInvoice();
                break;
            case 6:
                System.out.println("Sampai jumpa lagi!");
                System.exit(0);
                break;
            default:
                System.out.println("Oops, opsi tidak ditemukan.");
                home();
                break;
        }
    }

    private void getAllBarang() {
        BarangDao barangDao = new BarangDao();
        List<Barang> barangList = null;
        try {
            barangList = barangDao.getAllBarang();
        } catch (SQLException e) {
            // Gotta catch 'em all!
        }
        if (barangList.size() > 0) {
            PenjualDao penjualDao = new PenjualDao();
            Penjual penjual;
            List<Penjual> penjualList = new ArrayList<>();
            for (Barang barang : barangList) {
                try {
                    penjual = penjualDao.getPenjualFromBarangId(barang.getId());
                    penjualList.add(penjual);
                } catch (SQLException e) {
                    // Gotta catch 'em all!
                }
            }
            printBarangList(barangList, penjualList);
            getAllBarangSucceedOptions(barangList);
        } else {
            System.out.println("Oops, barang tidak ditemukan.");
            searchBarangFailedOptions();
        }
    }

    private void getAllBarangSucceedOptions(List<Barang> barangList) {
        System.out.println("----------------------------");
        System.out.println("(1) Tambahkan barang ke Cart");
        System.out.println("(2) Cari barang");
        System.out.println("(3) Kembali ke menu utama");
        System.out.println("----------------------------");
        System.out.println("Masukkan pilihan Anda:");
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();

        switch (input) {
            case 1:
                prepareToAddItems(barangList);
                break;
            case 2:
                searchBarang();
                break;
            case 3:
                home();
                break;
            default:
                System.out.println("Oops, opsi tidak ditemukan.");
                getAllBarangSucceedOptions(barangList);
                break;
        }
    }

    private void searchBarang() {
        BarangDao barangDao = new BarangDao();
        List<Barang> barangList = null;
        System.out.println("----------------------------");
        System.out.println("Masukkan kata kunci pencarian:");
        Scanner scanner = new Scanner(System.in);
        String keyword = scanner.nextLine();
        try {
            barangList = barangDao.getBarangFromName(keyword);
        } catch (SQLException e) {
            // Gotta catch 'em all!
        }
        if (barangList.size() > 0) {
            PenjualDao penjualDao = new PenjualDao();
            Penjual penjual;
            List<Penjual> penjualList = new ArrayList<>();
            for (Barang barang : barangList) {
                try {
                    penjual = penjualDao.getPenjualFromBarangId(barang.getId());
                    penjualList.add(penjual);
                } catch (SQLException e) {
                    // Gotta catch 'em all!
                }
            }
            printBarangList(barangList, penjualList);
            searchBarangSucceedOptions(barangList);
        } else {
            System.out.println("Oops, barang tidak ditemukan.");
            searchBarangFailedOptions();
        }
    }

    private void printBarangList(List<Barang> barangList, List<Penjual> penjualList) {
        for (int i = 0; i < barangList.size(); i++) {
            System.out.println("----------------------------");
            System.out.println("ID: " + barangList.get(i).getId());
            System.out.println("Nama: " + barangList.get(i).getNama());
            System.out.println("Harga: " + barangList.get(i).getHarga());
            System.out.println("Penjual: " + penjualList.get(i).getNama());
            System.out.println("----------------------------");
        }
    }

    private void searchBarangSucceedOptions(List<Barang> barangList) {
        System.out.println("(1) Tambahkan barang ke Cart");
        System.out.println("(2) Mencoba kata kunci pencarian lain");
        System.out.println("(3) Kembali ke menu utama");
        System.out.println("----------------------------");
        System.out.println("Masukkan pilihan Anda:");
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();

        switch (input) {
            case 1:
                prepareToAddItems(barangList);
                break;
            case 2:
                searchBarang();
                break;
            case 3:
                home();
                break;
            default:
                System.out.println("Oops, opsi tidak ditemukan.");
                searchBarangSucceedOptions(barangList);
                break;
        }
    }

    private void prepareToAddItems(List<Barang> barangList) {
        List<Integer> idList = new ArrayList<>();
        for (Barang barang : barangList) {
            idList.add(barang.getId());
        }
        Scanner scanner = new Scanner(System.in);
        System.out.println("Masukkan ID barang pilihan Anda:");
        int idInput = scanner.nextInt();
        int index = idList.indexOf(idInput);
        System.out.println("Masukkan jumlah item yang akan dibeli:");
        int jumlahInput = scanner.nextInt();
        try {
            addToCart(barangList.get(index), jumlahInput);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Oops, ID barang tidak ditemukan.");
            System.out.println("----------------------------");
            prepareToAddItems(barangList);
        }
    }

    private void addToCart(Barang barang, int jumlah) {
        Cart.getInstance().tambahItem(barang, jumlah);
        searchBarangFailedOptions();
    }

    private void searchBarangFailedOptions() {
        System.out.println("----------------------------");
        System.out.println("(1) Mencoba kata kunci pencarian lain");
        System.out.println("(2) Kembali ke menu utama");
        System.out.println("----------------------------");
        System.out.println("Masukkan pilihan Anda:");
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();

        switch (input) {
            case 1:
                searchBarang();
                break;
            case 2:
                home();
                break;
            default:
                System.out.println("Oops, opsi tidak ditemukan.");
                searchBarangFailedOptions();
                break;
        }
    }

    private void searchPenjual() {
        PenjualDao penjualDao = new PenjualDao();
        Penjual penjual = null;
        System.out.println("----------------------------");
        System.out.println("Masukkan kata kunci pencarian:");
        Scanner scanner = new Scanner(System.in);
        String keyword = scanner.nextLine();
        try {
            penjual = penjualDao.getPenjualFromNama(keyword);
        } catch (SQLException e) {
            // Gotta catch 'em all!
        }
        if (penjual != null) {
            printPenjual(penjual);
            searchPenjualSucceedOptions(penjual);
        } else {
            System.out.println("Oops, penjual tidak ditemukan.");
            searchPenjualFailedOptions();
        }
    }

    private void printPenjual(Penjual penjual) {
        System.out.println("----------------------------");
        System.out.println("Nama: " + penjual.getNama());
        System.out.println("Kontak: " + penjual.getKontak());
        System.out.println("Alamat: " + penjual.getAlamat());
        List<Barang> barangList = penjual.getBarangList();
        System.out.println("-------Barang-barang--------");
        for (Barang barang : barangList) {
            System.out.println("----------------------------");
            System.out.println("ID: " + barang.getId());
            System.out.println("Nama: " + barang.getNama());
            System.out.println("Harga: " + barang.getHarga());
            System.out.println("----------------------------");
        }
    }

    private void searchPenjualSucceedOptions(Penjual penjual) {
        System.out.println("(1) Tambahkan barang ke Cart");
        System.out.println("(2) Mencoba kata kunci pencarian lain");
        System.out.println("(3) Kembali ke menu utama");
        System.out.println("----------------------------");
        System.out.println("Masukkan pilihan Anda:");
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();

        switch (input) {
            case 1:
                prepareToAddItems(penjual.getBarangList());
                break;
            case 2:
                searchPenjual();
                break;
            case 3:
                home();
                break;
            default:
                System.out.println("Oops, opsi tidak ditemukan.");
                searchPenjualSucceedOptions(penjual);
                break;
        }
    }

    private void searchPenjualFailedOptions() {
        System.out.println("----------------------------");
        System.out.println("(1) Mencoba kata kunci pencarian lain");
        System.out.println("(2) Kembali ke menu utama");
        System.out.println("----------------------------");
        System.out.println("Masukkan pilihan Anda:");
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();

        switch (input) {
            case 1:
                searchPenjual();
                break;
            case 2:
                home();
                break;
            default:
                System.out.println("Oops, opsi tidak ditemukan.");
                searchPenjualFailedOptions();
                break;
        }
    }

    private void checkCartOptions() {
        System.out.println("(1) Kembali ke menu utama");
        System.out.println("(2) Checkout");
        System.out.println("----------------------------");
        System.out.println("Masukkan pilihan Anda:");
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();

        switch (input) {
            case 1:
                home();
                break;
            case 2:
                checkoutAndPrintInvoice();
                break;
            default:
                System.out.println("Oops, opsi tidak ditemukan.");
                checkCartOptions();
                break;
        }
    }

    private void checkoutAndPrintInvoice() {
        Invoice invoice = Cart.getInstance().checkout();
        invoice.printInvoice();
    }
}