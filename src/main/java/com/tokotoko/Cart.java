package com.tokotoko;

import com.tokotoko.dao.InvoiceDao;
import com.tokotoko.dao.PenjualDao;
import com.tokotoko.model.Barang;
import com.tokotoko.model.Invoice;
import com.tokotoko.model.Penjual;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by warpalajuri on 21/05/16.
 */
public enum Cart {
    INSTANCE;

    private int id;
    private LinkedHashMap<Barang, Integer> cartContent;
    private long total;
    private String namaPenjual;
    private String kontakPenjual;
    private String namaPembeli;
    private String alamatPembeli;
    private String kontakPembeli;

    public void initCart() {
        cartContent = new LinkedHashMap<>();
    }

    public void tambahItem(Barang barang, int jumlah) {
        cartContent.put(barang, jumlah);
    }

    public void hapusItem(Barang barang, int jumlah) {
        if (cartContent.get(barang) < jumlah) {
            System.out.println("Tidak dapat menghapus lebih dari barang yang ada.");
        }
        if (cartContent.get(barang) > jumlah) {
            cartContent.put(barang, jumlah);
        }
        if (cartContent.get(barang) == jumlah) {
            cartContent.remove(barang);
        }
    }

    public void checkContent() {
        this.total = calculateTotal();
        System.out.println("------------Cart------------");
        System.out.println("Item:");
        for (Map.Entry<Barang, Integer> map : cartContent.entrySet()) {
            String barang = map.getKey().getNama();
            int quantity = map.getValue();
            long itemPrice = map.getKey().getHarga();
            System.out.println(barang + " (Qty: " + quantity + " @" + itemPrice + ")");
        }
        System.out.println("Total: " + total);
        System.out.println("----------------------------");
    }

    public Invoice checkout() {
        System.out.println("Cart checkout.");
        getDataPenjual();
        inputDataPembeli();
        id = getLatestInvoiceIdAndIncrement();
        this.total = calculateTotal();
        Invoice invoice = new Invoice(id,
                cartContent,
                total,
                namaPenjual,
                kontakPenjual,
                namaPembeli,
                alamatPembeli,
                kontakPembeli);
        return invoice;
    }

    private Integer getLatestInvoiceIdAndIncrement() {
        InvoiceDao invoiceDao = new InvoiceDao();
        Integer id = null;
        try {
            id = invoiceDao.getLatestInvoiceId();
            id++;
        } catch (SQLException e) {
            // Gotta catch 'em all!
        }
        return id;
    }

    private void getDataPenjual() {
        PenjualDao penjualDao = new PenjualDao();
        Penjual penjual = null;
        int idBarang = cartContent.keySet().iterator().next().getId();
        try {
            penjual = penjualDao.getPenjualFromBarangId(idBarang);
        } catch (SQLException e) {
            // Gotta catch 'em all!
        }

        setNamaPenjual(penjual.getNama());
        setKontakPenjual(penjual.getKontak());
    }

    private void inputDataPembeli() {
        String stringInput;
        Scanner scannerInput = new Scanner(System.in);
        System.out.println("Input nama pembeli:");
        stringInput = scannerInput.nextLine();
        setNamaPembeli(stringInput);
        System.out.println("Input alamat pembeli:");
        stringInput = scannerInput.nextLine();
        setAlamatPembeli(stringInput);
        System.out.println("Input kontak pembeli:");
        stringInput = scannerInput.nextLine();
        setKontakPembeli(stringInput);
    }

    private long calculateTotal() {
        long total = 0;
        for (Map.Entry<Barang, Integer> map : cartContent.entrySet()) {
            long hargaBarang = map.getKey().getHarga();
            int quantity = map.getValue();
            long itemTotal = hargaBarang * quantity;
            total += itemTotal;
        }
        return total;
    }

    private void setNamaPenjual(String namaPenjual) {
        this.namaPenjual = namaPenjual;
    }

    private void setKontakPenjual(String kontakPenjual) {
        this.kontakPenjual = kontakPenjual;
    }

    private void setNamaPembeli(String namaPembeli) {
        this.namaPembeli = namaPembeli;
    }

    private void setAlamatPembeli(String alamatPembeli) {
        this.alamatPembeli = alamatPembeli;
    }

    private void setKontakPembeli(String kontakPembeli) {
        this.kontakPembeli = kontakPembeli;
    }

    public static Cart getInstance() {
        return Cart.INSTANCE;
    }
}

